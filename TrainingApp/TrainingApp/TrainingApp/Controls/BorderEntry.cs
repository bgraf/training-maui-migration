﻿using System;
using Xamarin.Forms;

namespace TrainingApp.Controls
{
	public class BorderEntry : Entry
	{
        public BindableProperty CustomBackgroundColorProperty = BindableProperty.Create(propertyName: nameof(CustomBackgroundColor),
                                                                                   returnType: typeof(Color),
                                                                                   declaringType: typeof(BorderEntry),
                                                                                   defaultValue: Color.AliceBlue,
                                                                                   defaultBindingMode: BindingMode.TwoWay);
        public Color CustomBackgroundColor
        {
            get => (Color)GetValue(CustomBackgroundColorProperty);
            set { SetValue(CustomBackgroundColorProperty, value); }
        }

        public BorderEntry()
		{
		}
	}
}

