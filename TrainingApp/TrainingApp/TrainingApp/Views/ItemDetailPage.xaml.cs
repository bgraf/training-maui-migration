﻿using System.ComponentModel;
using Xamarin.Forms;
using TrainingApp.ViewModels;

namespace TrainingApp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}
