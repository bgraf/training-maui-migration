﻿using System.ComponentModel;
using TrainingApp.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace TrainingApp.iOS.Renderer
{
    public class BorderEntryRenderer : EntryRenderer
	{
		public BorderEntryRenderer()
		{
		}

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == nameof(BorderEntry.CustomBackgroundColor)
                && Element is BorderEntry borderEntry)
            {

                borderEntry.HeightRequest = 40;
                Control.Layer.BorderWidth = 0;
                Control.BackgroundColor = borderEntry.CustomBackgroundColor.ToUIColor();
                Control.Layer.BorderColor = Color.Transparent.ToCGColor();
                Control.Layer.CornerRadius = 2;
            }
        }
    }
}