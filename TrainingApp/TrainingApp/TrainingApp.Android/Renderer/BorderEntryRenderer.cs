﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using TrainingApp.Controls;
using TrainingApp.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderEntry), typeof(BorderEntryRenderer))]
namespace TrainingApp.Droid.Renderer
{
    public class BorderEntryRenderer : EntryRenderer
    {
        public BorderEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == nameof(BorderEntry.CustomBackgroundColor)
                && Element is BorderEntry borderEntry)
            {
                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(borderEntry.CustomBackgroundColor.ToAndroid());
                gd.SetCornerRadius(10);
                gd.SetStroke(2, Color.LightGray.ToAndroid());
                Control.SetBackground(gd);
            }
        }
    }
}
